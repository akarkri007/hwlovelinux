# Extract hardware info from host with linux

## system

### system model, bios version and bios date, how old is the hardware?



	```
	[root@a15 ~]# dmidecode -t bios
	Version: F5
	Release Date: 01/17/2014
	```

## mainboard model, link to manual, link to product
```
[root@a15 ~]# dmidecode -s baseboard-product-name
H81M-S2PV
```
[link](https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.intel.la%2Fcontent%2Fwww%2Fxl%2Fes%2Fembedded%2Fproducts%2Fshark-bay%2Fdesktop%2Fspecifications.html&psig=AOvVaw16GDT-Fr-Waz9WwRNRwlHd&ust=1605686130526000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCICkmI2Nie0CFQAAAAAdAAAAABAE)

### memory banks (free or occupied)
```
[root@a15 ~]# lshw -class memory

  *-memory
       description: System Memory
       physical id: 7
       slot: System board or motherboard
       size: 8GiB
     *-bank:0
          description: DIMM DDR3 Synchronous 1400 MHz (0,7 ns)
          product: 9905584-014.A00LF
          vendor: Kingston
          physical id: 0
          serial: 54569422
          slot: ChannelA-DIMM0
          size: 4GiB
          width: 64 bits
          clock: 1400MHz (0.7ns)
     *-bank:1
          description: DIMM [empty]
          product: [Empty]
          vendor: [Empty]
          physical id: 1
          serial: [Empty]
          slot: ChannelA-DIMM1
     *-bank:2
          description: DIMM DDR3 Synchronous 1400 MHz (0,7 ns)
          product: 9905584-014.A00LF
          vendor: Kingston
          physical id: 2
          serial: 42561014
          slot: ChannelB-DIMM0
          size: 4GiB
          width: 64 bits
          clock: 1400MHz (0.7ns)
     *-bank:3
          description: DIMM [empty]
          product: [Empty]
          vendor: [Empty]
          physical id: 3
          serial: [Empty]
          slot: ChannelB-DIMM1
```

### how many disks and types can be connected










### chipset, link to 













## cpu

### cpu model, year, cores, threads, cache 
```
Nombre del modelo:                   Intel(R) Pentium(R) CPU G3250 @ 3.20GHz
Hilo(s) de procesamiento por núcleo: 1
Núcleo(s) por «socket»:              2
Caché L1d:                           64 KiB
Caché L1i:                           64 KiB
Caché L2:                            512 KiB
Caché L3:                            3 MiB

```


### socket 













## pci

### number of pci slots, lanes available
 










### devices connected
```
[root@a15 ~]# lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 465,8G  0 disk 
├─sda1   8:1    0     1K  0 part 
├─sda5   8:5    0   100G  0 part /
├─sda6   8:6    0   100G  0 part 
└─sda7   8:7    0     5G  0 part [SWAP]
```

### network device, model, kernel module, speed
```
	[root@a15 ~]# lshw -class network
    RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
```
```
[root@a15 ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 74:d4:35:a0:7a:35 brd ff:ff:ff:ff:ff:ff
    inet 10.200.246.215/24 brd 10.200.246.255 scope global dynamic noprefixroute enp2s0
       valid_lft 15945sec preferred_lft 15945sec
    inet6 fe80::da8b:3ac6:2da9:8967/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: virbr0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
    link/ether 52:54:00:41:e9:d4 brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.1/24 brd 192.168.122.255 scope global virbr0
       valid_lft forever preferred_lft forever
4: virbr0-nic: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel master virbr0 state DOWN group default qlen 1000
    link/ether 52:54:00:41:e9:d4 brd ff:ff:ff:ff:ff:ff
```


### audio device, model, kernel module
```
[root@a15 ~]# arecord -l
**** List of CAPTURE Hardware Devices ****
card 1: PCH [HDA Intel PCH], device 0: ALC887-VD Analog [ALC887-VD Analog]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 1: PCH [HDA Intel PCH], device 2: ALC887-VD Alt Analog [ALC887-VD Alt Analog]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
  ```



### vga device, model, kernel module








## hard disks


### /dev/* , model, bus type, bus speed







### test fio random (IOPS) and sequential (MBps)

